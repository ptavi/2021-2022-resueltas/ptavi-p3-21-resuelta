#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from os.path import splitext
from urllib.parse import urlparse
from urllib.request import urlretrieve
from xml.sax import parse

import smil


def retrieve(url: str, n: int) -> bool:
    """Retrieve url and store it in file<n>.<ext>

    Retrieves only if url starts with http:// or https://.
    Returns True if retrieved, False otherwise.
    <ext> is the extension of the url, if any.
    """
    if url.startswith('http://') or url.startswith('https://'):
        path = urlparse(url).path
        ext = splitext(path)[1]
        filename = f'fichero-{n}{ext}'
        print(f"Retrieving {url} into {filename}...")
        urlretrieve(url, filename)
        return True
    else:
        return False


def download(tags):
    count = 0
    for element in tags:
        for attr in element['attrs']:
            if attr[0] == 'src':
                result = retrieve(attr[1], count)
                if result:
                    count += 1


def main():
    """Programa principal"""
    if len(sys.argv) < 2:
        sys.exit("Usage: python3 download.py <file>")
    smil_handler = smil.SMILHandler()
    parse(open(sys.argv[1]), smil_handler)
    tags = smil_handler.get_tags()
    download(tags)


if __name__ == "__main__":
    main()
