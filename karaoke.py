#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import sys
from xml.sax import parse

import smil


def to_string(tags):
    string = ""
    for tag in tags:
        string += f"{tag['name']}"
        for attr in tag['attrs']:
            string += f'\t{attr[0]}="{attr[1]}"'
        string += '\n'
    return string


def to_json(tags):
    list = []
    for element in tags:
        item = {'name': element['name']}
        item['attrs'] = {}
        for attr in element['attrs']:
            item['attrs'][attr[0]] = attr[1]
        list.append(item)
    return json.dumps(list)


def main():
    """Programa principal"""
    if (len(sys.argv) > 1) and (sys.argv[1] == '--json'):
        sys.argv.pop(1)
        function = to_json
    else:
        function = to_string
    if len(sys.argv) < 2:
        sys.exit("Usage: python3 karaoke.py [--json] <file>")
    smil_handler = smil.SMILHandler()
    parse(open(sys.argv[1]), smil_handler)
    tags = smil_handler.get_tags()
    print(function(tags), end='')


if __name__ == "__main__":
    main()
