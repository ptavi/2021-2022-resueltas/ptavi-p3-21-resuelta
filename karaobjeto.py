#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
from os.path import splitext
import sys
from urllib.parse import urlparse
from urllib.request import urlretrieve
from xml.sax import parse

import smil


class Karaoke():

    def __init__(self, filename):
        smil_handler = smil.SMILHandler()
        parse(open(filename), smil_handler)
        self.tags = smil_handler.get_tags()

    def __str__(self):
        string = ""
        for tag in self.tags:
            string += f"{tag['name']}"
            for attr in tag['attrs']:
                string += f'\t{attr[0]}="{attr[1]}"'
            string += '\n'
        return string

    def to_json(self):
        list = []
        for element in self.tags:
            item = {'name': element['name']}
            item['attrs'] = {}
            for attr in element['attrs']:
                item['attrs'][attr[0]] = attr[1]
            list.append(item)
        return json.dumps(list)

    @staticmethod
    def _retrieve(url: str, n: int) -> bool:
        """Retrieve url and store it in file<n>.<ext>

        Retrieves only if url starts with http:// or https://.
        Returns True if retrieved, False otherwise.
        <ext> is the extension of the url, if any.
        """
        if url.startswith('http://') or url.startswith('https://'):
            path = urlparse(url).path
            ext = splitext(path)[1]
            filename = f'fichero-{n}{ext}'
            urlretrieve(url, filename)
            return True
        else:
            return False

    def download(self):
        count = 0
        for element in self.tags:
            for attr in element['attrs']:
                if attr[0] == 'src':
                    result = self._retrieve(attr[1], count)
                    if result:
                        count += 1


def main():
    """Programa principal"""
    if (len(sys.argv) > 1) and (sys.argv[1] == '--json'):
        sys.argv.pop(1)
        json = True
    else:
        json = False
    if len(sys.argv) < 2:
        sys.exit("Usage: python3 karaobjecto.py [--json] <file>")
    karaoke = Karaoke(sys.argv[1])
    if json:
        print(karaoke.to_json())
    else:
        print(karaoke, end='')
    karaoke.download()


if __name__ == "__main__":
    main()
