#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax.handler import ContentHandler


class SMILHandler(ContentHandler):
    """Clase para analizar documentos SMIL
    """

    elements = {
        'root-layout': ('width', 'height', 'background-color'),
        'region': ('id', 'top', 'bottom', 'left', 'right'),
        'img': ('src', 'region', 'begin', 'dur'),
        'audio': ('src', 'begin', 'dur'),
        'textstream': ('src', 'region')
    }

    def __init__(self):
        self.tags = []

    def startElement(self, name, attrs):
        if name in self.elements:
            tag = {'name': name,
                   'attrs': attrs.items()}
            self.tags.append(tag)

    def get_tags(self):
        return (self.tags)
